import json
import os
import re
import threading
import time

import cv2
import numpy as np
import pyautogui
from fpdf import FPDF
from PIL import Image
from desktopmagic.screengrab_win32 import getRectAsImage
import tkinter as tk
from tkinter import filedialog, messagebox, ttk, simpledialog

from lib.CustomWidgets import PopUpListbox, ConfigListbox


class ScriptRipper(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.title("ScriptRipper")
        self.resizable(False, False)

        container = tk.Frame(self)
        container.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        self.pages = (MainPage,)
        for F in self.pages:
            frame = F(container, self)
            frame.grid(row=0, column=0, sticky=tk.NSEW)
            self.frames[F] = frame

        self.show_frame(MainPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class MainPage(tk.Frame):
    next_nr = 1
    timer_started = False

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # Path Config
        self.path_lf = tk.LabelFrame(self, text="Pfad & Name")

        tk.Label(self.path_lf, text="Ordnerpfad: ").grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)
        self.path_E = tk.Entry(self.path_lf)
        self.path_E.grid(row=0, column=1, padx=5, pady=5)
        tk.Button(self.path_lf, text=" ... ", command=self.fill_path).grid(row=0, column=2, padx=5, pady=5)

        tk.Label(self.path_lf, text="Name: ").grid(row=1, column=0, padx=5, pady=5, sticky=tk.W)
        # self.name_E = tk.Entry(self.path_lf)
        # self.name_E.grid(row=1, column=1, padx=5, pady=5)
        # self.name_E.bind("<KeyRelease>", lambda e: self.update_page())

        self.name_E = ttk.Combobox(self.path_lf, width=17)
        self.name_E.grid(row=1, column=1, padx=5, pady=5)
        self.name_E.bind("<KeyRelease>", lambda e: self.update_page())

        # tk.Button(self.path_lf, text="...", command=self.get_existend_names).grid(row=1, column=2, padx=5, pady=5)

        tk.Label(self.path_lf, text="Nächste Seite: ").grid(row=2, column=0, padx=5, pady=5, sticky=tk.W)
        self.page_L = tk.Label(self.path_lf, text="1")
        self.page_L.grid(row=2, column=1, padx=5, pady=5)

        self.path_lf.pack(padx=5, pady=5, side=tk.TOP, expand=True, fill=tk.X)

        # Screenshot Config
        self.ssconfig_lf = tk.LabelFrame(self, text="Screenshot Config")

        tk.Label(self.ssconfig_lf, text="Oben Links: ").grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)
        self.ol_E = tk.Entry(self.ssconfig_lf, state="readonly")
        self.ol_E.grid(row=0, column=1, padx=5, pady=5)
        tk.Button(self.ssconfig_lf, text="↖ F10", state="disabled").grid(row=0, column=2, padx=5, pady=5)
        self.ol_E.bind("<F10>", lambda e: self.choose_point(self.ol_E))

        tk.Label(self.ssconfig_lf, text="Unten Rechts: ").grid(row=1, column=0, padx=5, pady=5, sticky=tk.W)
        self.ur_E = tk.Entry(self.ssconfig_lf, state="readonly")
        self.ur_E.grid(row=1, column=1, padx=5, pady=5)
        tk.Button(self.ssconfig_lf, text="↘ F10", state="disabled").grid(row=1, column=2, padx=5, pady=5)
        self.ur_E.bind("<F10>", lambda e: self.choose_point(self.ur_E))

        tk.Button(self.ssconfig_lf, text="Config Speichern", bg="#42c966", command=self.save_config).grid(
            row=3, column=0, padx=5, pady=5, sticky=tk.EW
        )

        self.load_config_cb = ttk.Combobox(self.ssconfig_lf, width=17, state="readonly")
        self.load_config_cb.grid(row=3, column=1, padx=5, pady=5, sticky=tk.EW)
        self.load_config_cb.set("Config laden")
        with open("etc/stored_areas.json") as f:
            data = json.load(f)
            configs = [_["name"] for _ in data]
        self.load_config_cb["values"] = configs
        self.load_config_cb.bind("<<ComboboxSelected>>", lambda e: self.load_config())

        # tk.Button(self.ssconfig_lf, text="Config Laden", bg="#42c966", command=self.load_config).grid(
        #     row=3, column=1, padx=5, pady=5, sticky=tk.EW
        # )

        self.blinker_B = tk.Button(self.ssconfig_lf, text="📷", bg="white", fg="black", state="disabled")
        self.blinker_B.grid(row=3, column=2, padx=5, pady=5, sticky=tk.EW)

        self.ssconfig_lf.grid_columnconfigure(0, weight=1)
        self.ssconfig_lf.pack(padx=5, pady=5, side=tk.TOP, expand=True, fill=tk.X)

        # Screenshot
        self.sshot_lf = tk.LabelFrame(self, text="Screenshot (F9)")

        nb = ttk.Notebook(self.sshot_lf)
        f1 = tk.Frame(nb)
        tk.Button(f1, text="Speichern", bg="#42c966",
                  command=lambda: self.take(int(self.page_L.cget("text")))).grid(
            row=0, column=0, padx=5, pady=5, sticky=tk.EW
        )
        controller.focus_set()
        controller.bind("<F9>", lambda e: self.take(int(self.page_L.cget("text"))))

        tk.Button(f1, text="Letztes Überschreiben", bg="#c97f42",
                  command=lambda: self.take(int(self.page_L.cget("text")) - 1)).grid(
            row=0, column=1, padx=5, pady=5, sticky=tk.EW
        )
        f1.pack(padx=5, pady=5, side=tk.TOP, expand=True, fill=tk.X)
        f1.grid_columnconfigure(0, weight=1)

        f2 = tk.Frame(nb)
        self.timer_B = tk.Button(f2, text="Start", bg="#42c966",
                                 command=lambda: self.start_timer(int(self.page_L.cget("text"))))
        self.timer_B.grid(row=0, column=0, padx=5, pady=5, sticky=tk.EW)

        self.timer_E = tk.Entry(f2)
        self.timer_E.grid(row=0, column=1, padx=5, pady=5, sticky=tk.EW)

        tk.Label(f2, text="sec.").grid(row=0, column=2, padx=5, pady=5, sticky=tk.EW)

        f2.pack(padx=5, pady=5, side=tk.TOP, expand=True, fill=tk.X)
        f2.grid_columnconfigure(0, weight=1)

        nb.pack(padx=5, pady=5, side=tk.TOP, expand=True, fill=tk.X)
        nb.add(f1, text="Normal")
        nb.add(f2, text="Timer")
        self.sshot_lf.pack(padx=5, pady=5, side=tk.TOP, expand=True, fill=tk.X)

        # PDF
        self.pdf_lf = tk.LabelFrame(self, text="PDF")

        tk.Button(self.pdf_lf, text="Erstelle PDF", bg="#4274c9", command=self.create_pdf).grid(
            row=1, column=0, padx=5, pady=5, columnspan=2, sticky=tk.EW
        )

        self.cb_var = tk.IntVar()
        self.cb = tk.Checkbutton(self.pdf_lf, text="Quellbilder löschen", variable=self.cb_var, onvalue=1, offvalue=0)
        self.cb.grid(row=0, column=0, padx=1, pady=5, sticky=tk.W)

        self.auto_choose_var = tk.IntVar()
        self.auto_choose_CB = tk.Checkbutton(self.pdf_lf, text="Dublikate löschen (Beta)",
                                             variable=self.auto_choose_var, onvalue=1, offvalue=0)
        self.auto_choose_CB.grid(row=0, column=1, padx=1, pady=5, sticky=tk.W)

        self.pdf_lf.grid_columnconfigure(0, weight=1)

        self.pdf_lf.pack(padx=5, pady=5, side=tk.TOP, expand=True, fill=tk.X)

    def fill_path(self):
        path = filedialog.askdirectory(initialdir="./Subjects")
        if path:
            self.path_E.delete(0, tk.END)
            self.path_E.insert(0, path)
            names = self.get_existend_names()
            self.name_E["values"] = list(names)

    def get_existend_names(self):
        path = self.path_E.get()

        if path:
            all_files_in_folder = os.listdir(path)

            matches = [re.search(f"(.+)_[0-9]+[.]png", _) for _ in all_files_in_folder if
                       re.search(f"(.+)_[0-9]+[.]png", _)]
            names = set(
                [_.group(1) for _ in matches]
            )

            #PopUpListbox(self.name_E, names, self.update_page)
            return names
        else:
            messagebox.showwarning("Fehler", "Bitte zuerst einen Ordnerpfad auswählen")

    def update_page(self):
        path = self.path_E.get()
        name = self.name_E.get()

        if path:
            all_files_in_folder = os.listdir(path)
            files_with_name = [re.search(f"{name}_([0-9]+)[.]png", _) for _ in all_files_in_folder if
                               re.search(f"{name}_([0-9]+)[.]png", _)]
            if files_with_name and name:
                highest_nr = max(
                    [int(_.group(1)) for _ in files_with_name]
                )
            else:
                highest_nr = 0

            self.next_nr = highest_nr + 1
        else:
            self.next_nr = 1

        self.page_L.config(text=str(self.next_nr))

    def save_config(self):
        ol = self.ol_E.get()
        ur = self.ur_E.get()

        if not (ol and ur):
            messagebox.showwarning("Fehler", "Keine Punkte ausgewählt")
            return

        name = simpledialog.askstring("Speichern", "Name der Konfiguration")

        if not name:
            return

        with open("etc/stored_areas.json", "r") as f:
            data = json.load(f)

        if not any([_["name"] == name for _ in data]):
            data.append({
                "name": name,
                "ol": ol,
                "ur": ur
            })

            configs = [_["name"] for _ in data]
            self.load_config_cb["values"] = configs

            with open("etc/stored_areas.json", "w") as f:
                json.dump(data, f)

            messagebox.showinfo("Gespeichert", "Die Konfiguration wurde gespeichert")
        else:
            messagebox.showerror("Fehler", "Dieser Name ist schon vorhanden")

    def load_config(self):
        selection = self.load_config_cb.get()

        with open("etc/stored_areas.json", "r") as f:
            data = json.load(f)

        # names = [_["name"] for _ in data]
        try:
            # selection = ConfigListbox(names).return_value()

            for i in data:
                if i["name"] == selection:
                    self.ol_E.config(state="normal")
                    self.ur_E.config(state="normal")
                    self.ol_E.delete(0, tk.END)
                    self.ur_E.delete(0, tk.END)
                    self.ol_E.insert(0, i["ol"])
                    self.ur_E.insert(0, i["ur"])
                    self.ol_E.config(state="readonly")
                    self.ur_E.config(state="readonly")
                    break

        except Exception as e:
            pass

    def choose_point(self, entry):
        entry.config(state="normal")
        point = pyautogui.position()
        entry.delete(0, tk.END)
        entry.insert(0, point)
        entry.config(state="readonly")

    def take(self, nr):
        path = self.path_E.get()
        name = self.name_E.get()
        ol = self.ol_E.get()
        ur = self.ur_E.get()

        if all((path, name, ol, ur)):
            fp = os.path.join(path, name) + f"_{nr}.png"
            try:
                ol = ol.split(" ")
                ur = ur.split(" ")
                rect256 = getRectAsImage((int(ol[0]), int(ol[1]), int(ur[0]), int(ur[1])))
                rect256.save(fp, format='png')

                self.update_page()
                self.blink()

            except Exception as e:
                messagebox.showerror("Fehler", f"Es ist ein Fehler aufgetreten: {e}")
        else:
            messagebox.showerror("Fehler", "Es ist ein Fehler aufgetreten")

    def start_timer(self, nr):
        try:
            delay = int(self.timer_E.get())
            self.timer_B.config(text="Stop", bg="red")
            threading.Thread(target=self.timer, args=(nr, delay)).start()
        except Exception as e:
            messagebox.showerror("Fehler", e)

    def timer(self, nr, delay):
        if self.timer_started:
            self.timer_B.config(text="Start", bg="gold")
            self.timer_started = False
        else:
            self.timer_started = True
            while self.timer_started:
                self.take(nr)
                self.update_page()
                nr += 1
                time.sleep(delay)

    def blink(self):
        def _blink():
            self.blinker_B.config(bg="black", fg="white")
            time.sleep(0.25)
            self.blinker_B.config(bg="white", fg="black")

        threading.Thread(target=_blink).start()

    def create_pdf(self):
        path = self.path_E.get()
        name = self.name_E.get()

        if all((path, name)):
            subject = os.path.basename(path)
            output_fp = filedialog.asksaveasfilename(initialdir="./Outputs", initialfile=f"{subject}_{name}.pdf")

            if not output_fp:
                return

            # Appending all Images corresponding to Path+Name
            all_imgs = []
            img_list = os.listdir(path)
            for file in img_list:
                if re.match(f"{name}_[0-9]+[.]png", file):
                    all_imgs.append(os.path.join(path, file))

            # Sorting and appending Images if they are not too similar
            imgs = []
            for file in sorted(all_imgs, key=lambda e: int(re.match(".+_([0-9]+)[.]png", e).group(1))):
                if self.auto_choose_var.get() == 1 and len(imgs) > 0:
                    print(f"Comparing {imgs[-1]} and {file}: ", end="")
                    img1 = cv2.imread(imgs[-1], 0)
                    img2 = cv2.imread(file, 0)

                    res = cv2.absdiff(img1, img2)
                    res = res.astype(np.uint8)
                    percentage = (np.count_nonzero(res) * 100) / res.size
                    print(percentage)

                    if percentage < 5.5:
                        continue

                imgs.append(os.path.join(path, file))

            if imgs:
                widths = []
                heights = []
                for img in imgs:
                    i = Image.open(img)
                    w, h = i.size
                    widths.append(w)
                    heights.append(h)

                orientation = "L" if max(widths) > max(heights) else "P"
                format = (max(heights) + 60, max(widths) + 60) if max(widths) > max(heights) else (
                    max(widths) + 60, max(heights) + 60
                )

                pdf = FPDF(orientation, "pt", format)
                pdf.set_auto_page_break(False)

                try:
                    # for img in sorted(imgs, key=lambda e: int(re.match(".+_([0-9]+)[.]png", e).group(1))):
                    for img in imgs:
                        print(img)
                        pdf.add_page()
                        pdf.image(img)
                    pdf.output(output_fp, "F")

                    messagebox.showinfo("PDF", "PDF wurde generiert!")

                except Exception as e:
                    messagebox.showerror("Fehler", e)

                if self.cb_var.get() == 1:
                    for img in imgs:
                        try:
                            os.unlink(img)
                        except Exception as e:
                            pass
        else:
            messagebox.showerror("Fehler", "Kein Pfad/Name festgelegt!")


if __name__ == '__main__':
    sr = ScriptRipper()
    sr.mainloop()
