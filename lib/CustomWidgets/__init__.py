import tkinter as tk


class PopUpListbox(tk.Toplevel):
    def __init__(self, target_widget, values, update_callback, *args, **kwargs):
        tk.Toplevel.__init__(self, *args, **kwargs)
        self.title("Bereits vorhandene Namen")
        self.geometry("200x200")

        self.lb = tk.Listbox(self)
        for item in values:
            self.lb.insert(tk.END, item)

        self.lb.pack(padx=5, pady=5, expand=True, fill=tk.BOTH)

        self.lb.bind("<<ListboxSelect>>", lambda e: self.insert(target_widget, update_callback))

    def insert(self, widget, update_callback):
        cs = self.lb.get(self.lb.curselection())
        widget.delete(0, tk.END)
        widget.insert(0, cs)
        update_callback()

        self.destroy()

class ConfigListbox(tk.Toplevel):
    def __init__(self, values, *args, **kwargs):
        tk.Toplevel.__init__(self, *args, **kwargs)
        self.title("Bereits vorhandene Namen")
        self.geometry("200x200")

        self.lb = tk.Listbox(self)
        for item in values:
            self.lb.insert(tk.END, item)

        self.lb.pack(padx=5, pady=5, expand=True, fill=tk.BOTH)

        self.lb.bind("<<ListboxSelect>>", lambda e: self.get_selection())

    def get_selection(self):
        self.value = self.lb.get(self.lb.curselection())
        self.destroy()

    def return_value(self):
        self.wait_window()
        return self.value
